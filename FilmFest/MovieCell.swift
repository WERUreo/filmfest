//
//  MovieCell.swift
//  FilmFest
//
//  Created by Keli'i Martin on 2/4/17.
//  Copyright © 2017 WERUreo. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    func configMovieCell(withMovie: Movie) {
        self.textLabel?.text = withMovie.title
        self.detailTextLabel?.text = withMovie.releaseDate
    }

}
