//
//  MovieLibraryViewController
//  FilmFest
//
//  Created by Keli'i Martin on 1/31/17.
//  Copyright © 2017 WERUreo. All rights reserved.
//

import UIKit
import ChameleonFramework

class MovieLibraryViewController: UIViewController {

    @IBOutlet weak var movieTableView: UITableView!
    @IBOutlet var dataService: MovieLibraryDataService!

    var movieManager = MovieManager()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = GradientColor(.radial, frame: self.view.frame, colors: [UIColor.flatSkyBlue(), UIColor.flatNavyBlue()])

        self.movieTableView.dataSource = dataService
        self.movieTableView.delegate = dataService
        dataService.movieManager = movieManager

        dataService.movieManager?.addMovieToLibrary(movie: Movie(title: "Action", releaseDate: "02/03/1999"))
        dataService.movieManager?.addMovieToLibrary(movie: Movie(title: "Suspense", releaseDate: "02/03/1999"))
        dataService.movieManager?.addMovieToLibrary(movie: Movie(title: "Thriller", releaseDate: "02/03/1999"))
        dataService.movieManager?.addMovieToLibrary(movie: Movie(title: "Sci-fi", releaseDate: "02/03/1999"))
        dataService.movieManager?.addMovieToLibrary(movie: Movie(title: "Documentary", releaseDate: "02/03/1999"))

        self.movieTableView.reloadData()
    }
}

